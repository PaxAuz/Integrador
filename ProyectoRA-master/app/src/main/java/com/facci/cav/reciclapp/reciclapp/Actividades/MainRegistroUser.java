package com.facci.cav.reciclapp.reciclapp.Actividades;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.facci.cav.reciclapp.reciclapp.Modelo.Usuario;
import com.facci.cav.reciclapp.reciclapp.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class MainRegistroUser extends AppCompatActivity implements View.OnClickListener{
    private EditText Correo, Nombre, Apellido, Telefono, Direccion, Password, vPassword;
    private Button Registr;
    private ProgressDialog progressDialog;
    private FirebaseDatabase firebaseDatabase;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference usuario;
    private ImageView imageView;
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_registro_user);

        Correo = (EditText) findViewById(R.id.txtCorreo);
        Nombre = (EditText) findViewById(R.id.txtNombre);
        Apellido = (EditText) findViewById(R.id.txtApellido);
        Telefono = (EditText) findViewById(R.id.txtTelefono);
        Direccion = (EditText) findViewById(R.id.txtDireccion);
        Password = (EditText) findViewById(R.id.txtPassword);
        vPassword = (EditText) findViewById(R.id.txtVPassword);
        Registr = (Button) findViewById(R.id.btnRegistrarse);
        progressDialog = new ProgressDialog(this);
        firebaseDatabase = FirebaseDatabase.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();
        usuario = firebaseDatabase.getReference("TABLA DE USUARIOS");
        imageView = (ImageView)findViewById(R.id.IMVPerfil);
        Registr.setOnClickListener(this);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainRegistroUser.this, "HOLA", Toast.LENGTH_LONG).show();
            }
        });

    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btnRegistrarse:

                if (Correo.getText().toString().isEmpty()) {
                    Correo.setError(getString(R.string.campoEmail));
                    Correo.requestFocus();
                } else if (Nombre.getText().toString().isEmpty()) {
                    Nombre.setError(getString(R.string.campoNombre));
                } else if (Apellido.getText().toString().isEmpty()) {
                    Apellido.setError(getString(R.string.campoApellido));
                } else if (Telefono.getText().toString().isEmpty()) {
                    Telefono.setError(getString(R.string.campoTelefono));
                } else if (Direccion.getText().toString().isEmpty()) {
                    Direccion.setError(getString(R.string.campoDireccion));
                } else if (Password.getText().toString().isEmpty()) {
                    Password.setError(getString(R.string.campoPassword));
                } else if (vPassword.getText().toString().isEmpty()) {
                    vPassword.setError(getString(R.string.campoVerifPassw));
                }else if (Password.getText().toString().equals(vPassword.getText().toString())==false){
                    vPassword.setError(getString(R.string.campoValidacion));

                }else {
                    progressDialog.setTitle(getString(R.string.tittleRegistro));
                    progressDialog.setMessage(getString(R.string.mensajeRegistro));
                    progressDialog.show();
                    firebaseAuth.createUserWithEmailAndPassword(Correo.getText().toString(), Password.getText().toString())
                            .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                                @Override
                                public void onSuccess(AuthResult authResult) {

                                    Usuario User = new Usuario();
                                    User.setNombre(Nombre.getText().toString());
                                    User.setApellido(Apellido.getText().toString());
                                    User.setTelefono(Telefono.getText().toString());
                                    User.setDireccion(Direccion.getText().toString());
                                    User.setCorreo(Correo.getText().toString());
                                    User.setPassword(Password.getText().toString());
                                    User.setVerificacionPassword(vPassword.getText().toString());

                                    usuario.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).setValue(User)
                                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            Toast.makeText(MainRegistroUser.this, getString(R.string.toastRegistroUsuario), Toast.LENGTH_LONG).show();
                                            Intent intent = new Intent(MainRegistroUser.this, MainLogin.class);
                                            startActivity(intent);
                                        }
                                    }).addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Toast.makeText(MainRegistroUser.this, getString(R.string.toastErrorRegistro), Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                            });
                }
            break;
        }
    }
}
