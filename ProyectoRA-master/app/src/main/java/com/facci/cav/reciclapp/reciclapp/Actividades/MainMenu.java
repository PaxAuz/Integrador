package com.facci.cav.reciclapp.reciclapp.Actividades;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.facci.cav.reciclapp.reciclapp.Modelo.Usuario;
import com.facci.cav.reciclapp.reciclapp.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MainMenu extends AppCompatActivity implements View.OnClickListener{

    private Button mperfil,minformacion,mreciclaje, mRegistros, Salir;
    private TextView bienvenido;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference usuario;
    private String userId;
    private FirebaseAuth.AuthStateListener authStateListener;
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        mperfil=(Button)findViewById(R.id.btnMU);
        mperfil.setOnClickListener(this);
        minformacion=(Button)findViewById(R.id.btnINF);
        minformacion.setOnClickListener(this);
        mRegistros=(Button)findViewById(R.id.btnRGT);
        mRegistros.setOnClickListener(this);
        mreciclaje=(Button)findViewById(R.id.btnRCJ);
        mreciclaje.setOnClickListener(this);
        bienvenido = (TextView)findViewById(R.id.txtUserB);
        Salir = (Button)findViewById(R.id.BtnSalir);
        Salir.setOnClickListener(this);
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance();
        usuario = firebaseDatabase.getReference();
        FirebaseUser user = firebaseAuth.getCurrentUser();
        userId = user.getUid();

        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {

            }
        };

        usuario.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Bienvenido(dataSnapshot);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void Bienvenido(DataSnapshot dataSnapshot){
        for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()){
            Usuario usuario1 = new Usuario();
            usuario1.setNombre(dataSnapshot1.child(userId).getValue(Usuario.class).getNombre());
            usuario1.setApellido(dataSnapshot1.child(userId).getValue(Usuario.class).getApellido());

            bienvenido.setText(getString(R.string.mensajeBienvenido)+" " + usuario1.getNombre() + " " + usuario1.getApellido());



        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.BtnSalir:
                firebaseAuth.signOut();
                startActivity(new Intent(MainMenu.this, MainLogin.class));
                finish();
                break;
            case R.id.btnINF:
                startActivity(new Intent(MainMenu.this, MainInformacion.class));
                break;
            case R.id.btnRGT:
                startActivity(new Intent(MainMenu.this, MainRegistros.class));
                break;
            case R.id.btnRCJ:
                startActivity(new Intent(MainMenu.this, MainReciclaje.class));
                break;
            case R.id.btnMU:
                startActivity(new Intent(MainMenu.this, MainUsuario.class));
                break;
        }

    }
}
